#!/Users/xdyme/.sdkman/candidates/groovy/current/bin/groovy

BigDecimal number = new BigDecimal(args[0])

println new Worker().permuteAndAdd(number)

class Worker {
  BigDecimal permuteAndAdd(BigDecimal num) {
    // 123 -> [1, 2, 3]
    List<BigDecimal> nums = _getPlaceValueNums(num, [])

    // See method for details, but in essence:
    // 1. partition number: 123 -> [[1, 23], [12, 3], [123]]
    // 2. permute them so all possible variations of the above list members
    //    are obtained: [[[1, 23], [1, 32]], [12, 3], [21, 3]]
    List<List<List<String>>> permutations = _permutationsOf(num)
    // println "perms           -> $permutations"

    // Flatten simply to a list of lists, each list item a standalone permutation
    // Input: [[[1, 32], [1, 23]], [[21, 3], [12, 3]], [[123], [321], [213], [312], [132], [231]]]
    // Output: [[1, 32], [1, 23], [21, 3], [12, 3], [123], [321], [213], [312], [132], [231]]
    List<List<String>> flattenedPerms = [] 
    permutations.each { l -> l.each { flattenedPerms << it } }
    // println "flatperms       -> $flattenedPerms"

    // Convert to BigDecimals
    List<List<BigDecimal>> bigDecimalPerms = flattenedPerms.collectNested { 
      new BigDecimal(it)
    }
    // println "bigDecimalPerms -> $bigDecimalPerms"

    // Finally, recursively add and reduce to single digit each sublist item
    Set<BigDecimal> additionResults = bigDecimalPerms.collect(new HashSet()) {
      def result = _recursiveAdd(it.sum())
      // println "Recursive addition of $it -> $result"
      result
    }

    assert additionResults.size() == 1
    assert additionResults[0] == _recursiveAdd(num)
    additionResults[0]
  }

  private BigDecimal _recursiveAdd(BigDecimal num) {
    BigDecimal reduced = _getPlaceValueNums(num, []).sum()
    return reduced < 10 ? reduced : _recursiveAdd(reduced)
  }

  /** 
   * Could have just operated on a String representation, simpler! But was 
   * feeling adventurous!
   */ 
  private List<BigDecimal> _getPlaceValueNums(BigDecimal num, List<BigDecimal> accum) {
    if (num <= 0) {
      return accum.empty ? (accum << 0) : accum
    } else {
      BigDecimal tensVal = num.remainder(10)      
      BigDecimal nextNum = (num - tensVal) / 10
      return _getPlaceValueNums(nextNum, accum << tensVal)
    }
  }

  private List<List<String>> _permutationsOf(BigDecimal num) {
    String numStr = num.toString()

    // partition number into all possible partitions
    // "123" -> [[1, 23], [12, 3], [123]]
    List<List<String>> partitions = (0..<numStr.length()).collect { int idx ->
      def result = [numStr.substring(0, idx + 1), numStr.substring(idx + 1)]
      // clear out empty strings
      result.findAll { it != "" }      
    }
    
    // now expand each number in a partition to its permutations, and combine them
    // input: [[1, 23], [12, 3], [123]]
    // output:[
    //    /* perms of [1, 23] */ [1, 23], [1, 32], 
    //    /* perms of [12, 3] */ [12, 3], [21, 3],
    //    /* perms of   [123] */ 123], [321], [213], [312], [132], [231]
    // ]
    // note the combinations that can occur for partitions where each component
    // has values > 9, e.g. [12, 16] -> [[12, 16], [61, 21]].combinations()
    List<List<String>> permutations = partitions.collect { List<String> part ->
      assert part.size() in [1, 2]

      def _perm = { String s -> 
        s.collect()               // chars
         .permutations()          // permutation of chars
         .collect { it.join("") } // combine chars again
      }

      def lstToCombine = part.size() == 2 ?
        [_perm(part[0]), _perm(part[1])] :
        [_perm(part[0])]

      def combs = lstToCombine.combinations().collect { it.flatten() }
      // println "! ${combs} vs. ${combs.flatten()}"
      combs
    }

    permutations

  }
}
